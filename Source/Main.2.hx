package;

import lime.app.Application;
import lime.utils.Log;

import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;
import lime.graphics.Image;

import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLBuffer;
import lime.graphics.opengl.GLTexture;
import lime.graphics.opengl.GLProgram;
import lime.graphics.opengl.GLUniformLocation;

import lime.math.Vector4;
import lime.math.Matrix4;

import lime.utils.Float32Array;
import lime.utils.Assets;
import lime.utils.AssetType;


class Main extends Application {

    private var minX:Int;
    private var minY:Int;
    private var maxX:Int;
    private var maxY:Int;

    private var program : GLProgram;
    private var positionAttributeHandle : Int;
    private var textureUVAttributeHandle : Int;

    private var vertexBufferHandleA : GLBuffer;
    private var vertexBufferHandleB : GLBuffer;

    private var triangleColorUniform : GLUniformLocation;
    private var matrixUniform : GLUniformLocation;
    private var textureUniform : GLUniformLocation;
	// var private textureUnit : Int = 0;
    private var textureUnit : Int = 1;
    private var textureHandle : GLTexture;

    private var rotationMatrixTriangleA : Matrix4;
    private var rotationPointTriangleA : Vector4;
    private var identityMatrix : Matrix4;

    public function new () {

	    super ();

	    trace ("OpenGL Tutorial #9");
	    Log.info ("OpenGL Tutorial #9");
	}

    public override function onRenderContextLost ():Void {
	    trace ("onRenderContextLost");
	}

    public override function onRenderContextRestored (context:RenderContext):Void {
	    trace ("onRenderContextRestored");
	}

    public override function onPreloadComplete ():Void {
	    trace ("onPreloadComplete");
	    trace(Assets.list(AssetType.IMAGE));
	    appInit();
	}

    private function appInit():Void {

		// set here or (better yet) in project.xml: <window width="600" height="600" />
		// window.width = 600;
		// window.height = 600;

	    minX = 0;
	    maxX = window.width;
	    minY = 0;
	    maxY = window.height;

	    init();


	    switch (window.context.type) {

		    case CAIRO:
			    Log.warn ("Render context not supported: CAIRO");

		    case CANVAS:
			    Log.warn ("Render context not supported: CANVAS");

		    case DOM:
			    Log.warn ("Render context not supported: DOM");

		    case FLASH:
			    Log.warn ("Render context not supported: FLASH");

		    case OPENGL, OPENGLES, WEBGL:

			    initShader(window.context);
			    initGL(window.context);

		    default:

			    Log.warn ("Current render context not supported by this sample");

		}
	}

    private function init():Void {
		// by default, both matrices are set to identity matrix
	    rotationMatrixTriangleA = new Matrix4();
	    identityMatrix = new Matrix4();

	    rotationPointTriangleA = new Vector4();
	}

    private function initShader (context:RenderContext):Void {

	    var gl = context.webgl;

	    var vertexShaderSource = "
			// #version 120
		    attribute vec3 position;
		    attribute vec2 aTextureUV;

		    uniform mat4 transformMatrix;

		    varying vec2 vTextureUV;

		    void main (void) {
			    vTextureUV = aTextureUV;
			    gl_Position = transformMatrix * vec4(position, 1.0);
			}
		";

	    var fragmentShaderSource = 
			#if (!desktop || rpi)
			"precision mediump float;" +
			#end
			"
			// #version 120
		    varying vec2 vTextureUV;

		    uniform vec3 triangleColor;
		    uniform sampler2D texture;

		    void main (void) {
			    gl_FragColor = vec4(triangleColor, 1.0);
				// gl_FragColor = vec4(triangleColor, 1.0) * texture2D (texture, vTextureUV);
			}
		";

	    program = GLProgram.fromSources (gl, vertexShaderSource, fragmentShaderSource);
		// NOT 'use'-ing ;) it here, we need to switch programs while we draw
		// so we'll call gl.useProgram() in render()
		// gl.useProgram (program);
	    positionAttributeHandle = gl.getAttribLocation( program, "position" );
	    textureUVAttributeHandle = gl.getAttribLocation( program, "aTextureUV" );

        // grab program location of the uniform for triangle color
        triangleColorUniform = gl.getUniformLocation( program, "triangleColor" );
		// grab program location of the uniform for transformation matrix
	    matrixUniform = gl.getUniformLocation( program, "transformMatrix" );

	    textureUniform = gl.getUniformLocation( program, "texture" );
	}

    private function initGL(context:RenderContext):Void {

	    var gl = context.webgl;


	#if (lime_opengl || lime_opengles)
	    trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
	    trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
	#end

		//
		// global GL context setup
		//
	    gl.clearColor(63.0/255.0, 127.0/255.0, 191.0/255.0, 1.0);  // Bluish
		// comment out this line to make triangle A appear behind triangle B when it is rotating
	    gl.enable(GL.DEPTH_TEST);
	    gl.clearDepth(1.0); // set clear depth value to farthest


		//
		// data setup
		//
        var triangle_A_buffer_data : Array<Float> = [
			// point 1
			-1.0, -1.0, -0.5, // NEGATIVE z-coordinate values means this triangle is CLOSER
			// point 2
		    0.0, -1.0, -0.5,
			// point 3
			-0.5,  1.0, -0.5,
		];
        zoom(triangle_A_buffer_data, 0.5);
		// we want rotation to happen around point 1 of triangle A
	    rotationPointTriangleA = new Vector4(
		    triangle_A_buffer_data[0],
		    triangle_A_buffer_data[1],
		    triangle_A_buffer_data[2],
		    0.0
		);
        vertexBufferHandleA = createAndUploadVertexBuffer(gl, triangle_A_buffer_data);


        // attribute format:
        // X, Y, Z, U, V
        var triangle_B_buffer_data : Array<Float> = [
			// vertex 0
			-0.5, -0.5, 0.0, 0.0, 1.0,
			// vertex 1
			-0.5, 0.5, 0.0, 0.0, 0.0,
			// vertex 2
		    0.5, -0.5, 0.0, 1.0, 1.0,
			// vertex 2
		    0.5, -0.5, 0.0, 1.0, 1.0,
			// vertex 1
			-0.5, 0.5, 0.0, 0.0, 0.0,
			// vertex 3
		    0.5, 0.5, 0.0, 1.0, 0.0,
		];
        // var triangle_B_buffer_data : Array<Float> = [
		// 	// vertex 0
		// 	-0.5, -0.5, 0.0, 0.0, 1.0,
		// 	// vertex 1
		// 	-0.5, 0.5, 0.0, 0.0, 0.0,
		// 	// vertex 2
		//     0.5, -0.5, 0.0, 1.0, 1.0,
		// 	// vertex 3
		//     0.5, 0.5, 0.0, 1.0, 0.0,
		// ];
        // var squareAElementsBuffer : Array<Int> = [
		// 	// triangle 0
		//     0, 1, 2,
		// 	// triangle 1
		//     2, 1, 3,
		// ];
        // zoom(triangle_B_buffer_data, 0.5);
        vertexBufferHandleB = createAndUploadVertexBuffer(gl, triangle_B_buffer_data);

	    var image = Assets.getImage ("assets/UV_Grid_1024.jpeg");

	    textureHandle = createAndUploadTextureBuffer( gl, image );
	}

    public override function update (deltaTime:Int):Void {

	    if (!preloader.complete) return;

		// deltaTime is in milliseconds
	    var rotationSpeed = 90.0; // in degrees per second
	    var delta = deltaTime / 1000.0;

	    var rp = rotationPointTriangleA;
	    rotationMatrixTriangleA.appendTranslation( -rp.x, -rp.y, -rp.z );
	    rotationMatrixTriangleA.appendRotation( delta * rotationSpeed, Vector4.Z_AXIS );
	    rotationMatrixTriangleA.appendTranslation( rp.x, rp.y, rp.z );
	}

    public override function render (context:RenderContext):Void {

	    if (!preloader.complete) return;

	    switch (context.type) {

		    case OPENGL, OPENGLES, WEBGL:

			    var gl = context.webgl;

			    gl.viewport( 0, 0, maxX, maxY );
			    gl.clear( GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT );


				//
				// START
				//

				// calculate angle we will use to animate color and rotation
			    var period = 4000.0;
			    var theta = ( ( Sys.time() * 1000.0 ) % period ) / period;

				// Both triangles use the same program
				// "Acitvate" the 'position' attribute in our shader program
			    gl.enableVertexAttribArray( positionAttributeHandle );
				// gl.enableVertexAttribArray( textureUVAttributeHandle );
				// "Activate" our shader program
			    gl.useProgram( program );

				//
				// Triangle A
				//
				// set triangle A color
		        gl.uniform3f( triangleColorUniform, 0.5, 1.0, 0.0 );
				// Bind our triangle A vertex buffer to the current context
			    gl.bindBuffer( GL.ARRAY_BUFFER, vertexBufferHandleA );
				// "Describe" the attribute structure
			    gl.vertexAttribPointer( positionAttributeHandle, 3, GL.FLOAT, false, 0, 0 );
				// set transformation matric for triangle A
			    gl.uniformMatrix4fv( matrixUniform, false, rotationMatrixTriangleA );
				// Draw the triangle A
			    gl.drawArrays( GL.TRIANGLES, 0, 3 );
				// once done, disable 'position' attribute array so it doesn't conflict with next program, if any.
				// gl.disableVertexAttribArray( positionAttributeHandle );
				// gl.disableVertexAttribArray( textureUVAttributeHandle );

				//
				// Triangle B, aka textured quad
				//
				// gl.enableVertexAttribArray ( positionAttributeHandle );
				// gl.enableVertexAttribArray( textureUVAttributeHandle );
			    gl.activeTexture( GL.TEXTURE0 + textureUnit );
			    gl.bindTexture( GL.TEXTURE_2D, textureHandle );
				#if desktop
			    gl.enable (GL.TEXTURE_2D);
				#end

			    gl.bindBuffer( GL.ARRAY_BUFFER, vertexBufferHandleB );
			    defineProgramAttributesForQuad( gl );
				// gl.vertexAttribPointer( positionAttributeHandle, 3, GL.FLOAT, false, 0, 0 );
				// set transformation matrix for triangle B
			    gl.uniformMatrix4fv( matrixUniform, false, identityMatrix );
		        gl.uniform1i( textureUniform, textureUnit );

				// set quad color
			    var red = ( Math.sin( theta * Math.PI * 2 ) + 1.0 ) / 2.0;
		        gl.uniform3f( triangleColorUniform, red, 0.0, 1.0 );
			    gl.drawArrays( GL.TRIANGLES, 0, 6 );

				//
				// END
				//
			    gl.disableVertexAttribArray( positionAttributeHandle );
			    gl.disableVertexAttribArray( textureUVAttributeHandle );

		    default:

		}
	}

    private function defineProgramAttributesForQuad( gl : WebGLRenderContext ) : Void {
        // describe where the value for this attribute is in the vertex data buffer
        // and what is it's structure ( 3-component float vector, in our case )
        gl.vertexAttribPointer(
            // attribute handle (ID)
            positionAttributeHandle,
            // the number of components per generic vertex attribute. Must be 1, 2, 3, 4. 
            3,
            // the data type of each component in the array
            // The symbolic constants GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, and GL_UNSIGNED_INT are accepted by glVertexAttribPointer and glVertexAttribIPointer
            GL.FLOAT,
            // normalized or not
            false,
            // the byte offset between consecutive generic vertex attributes.
            // this is basically the number of bytes all attributes for one vertex take up
            // in our case we have first attribute (position) with 3 components (x,y,z) each of type GL_FLOAT, 
            // and second attribute (texcoord) with 2 components (u,v) each of type GL_FLOAT, so it would be 5 * Float.BYTES.
            5 * Float32Array.BYTES_PER_ELEMENT, // 5 * Float.BYTES,
            // an offset of the first component of the first generic vertex attribute in the array buffer
            // "position" attribute starts right at the start - 0
            0
        );

        // describe where the value for this attribute is in the vertex data buffer
        // and what is it's structure ( 2-component float vector, in our case )
        gl.vertexAttribPointer(
            // attribute handle (ID)
            textureUVAttributeHandle,
            // the number of components per generic vertex attribute. Must be 1, 2, 3, 4. 
            2,
            // the data type of each component in the array
            // The symbolic constants GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, and GL_UNSIGNED_INT are accepted by glVertexAttribPointer and glVertexAttribIPointer
            GL.FLOAT,
            // normalized or not
            false,
            // the byte offset between consecutive generic vertex attributes.
            // this is basically the number of bytes all attributes for one vertex take up
            // in our case we have first attribute (position) with 3 components (x,y,z) each of type GL_FLOAT, 
            // and second attribute (texcoord) with 2 components (u,v) each of type GL_FLOAT, so it would be 5 * Float.BYTES.
            5 * Float32Array.BYTES_PER_ELEMENT, // 5 * Float.BYTES,
            // an offset of the first component of the first generic vertex attribute in the array buffer
            // "texcoord" attribute starts right after the "position", which takes up 3 * Float.BYTES bytes, hence the value used
            3 * Float32Array.BYTES_PER_ELEMENT // 3 * Float.BYTES
        );
    }

    private function createAndUploadTextureBuffer( gl : WebGLRenderContext, image : Image ) : GLTexture {
        // no mipmap for this example
        var mipMap = false;
        var imageWidth = image.buffer.width;
        var imageHeight = image.buffer.height;
        // ByteBuffer pixelsBuffer = convertToRGBABuffer(image);
        var alpha = true;

        // must be one of GL_ALPHA, GL_RGB, GL_RGBA, GL_LUMINANCE, and GL_LUMINANCE_ALPHA
        var type = alpha ? GL.RGBA : GL.RGB;

	    var texture : GLTexture = gl.createTexture();

        // By default, glBindTexture binds our texture to texture unit 0
        // Just for the sake of clarity, we will explicitly set the texture unit to use, unit 0
        // this texture unit will be bound to the first declared sampler2D uniform in your fragment shader.
        gl.activeTexture( GL.TEXTURE0 + textureUnit );
	    gl.bindTexture( GL.TEXTURE_2D, texture );

        // when scaling down
        // use GL2ES2.GL_NEAREST or GL2ES2.GL_NEAREST_MIPMAP_LINEAR for pixelated look
        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, mipMap ? GL.LINEAR_MIPMAP_LINEAR : GL.LINEAR);
        // when scaling up
        // use GL2ES2.GL_NEAREST or GL2ES2.GL_NEAREST_MIPMAP_LINEAR for pixelated look
        gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, mipMap ? GL.LINEAR_MIPMAP_LINEAR : GL.LINEAR);
		// gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		// gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		// gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE );
		// gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE );

        // This effectively uploads the texture data (pixels) to GPU
        gl.texImage2D (
            GL.TEXTURE_2D, 
            0, 
            type,
            imageWidth, 
            imageHeight, 
            0, 
            type,
            GL.UNSIGNED_BYTE,
            image.data // pixelsBuffer
        );

        // generate mipmaps AFTER image data has been loaded to GPU
        if( mipMap ) {
            gl.generateMipmap( GL.TEXTURE_2D );
        }

	    gl.bindTexture( gl.TEXTURE_2D, null );

	    return texture;
	}

    private function createAndUploadVertexBuffer( gl:WebGLRenderContext, a_buffer_data : Array<Float> ):GLBuffer {

	    var bufferData:Float32Array = new Float32Array (a_buffer_data);

        // transfer vertex data to GPU memory
        // this way we don't have to keep buffers in RAM and transfer them on every frame render

        // Generate 1 buffer, put the resulting identifier in vertexbufferHandles
	    var bufferHandle:GLBuffer = gl.createBuffer (); // == glGenBuffers( 1, &bufferHandle )

        // this sets the type of data we want to upload to GPU
        // GL_ARRAY_BUFFER is for vertices
        // GL_ELEMENT_ARRAY_BUFFER is for geometry elements, etc.
        // gl.glBindBuffer( GL2ES2.GL_ARRAY_BUFFER, bufferHandle );
	    gl.bindBuffer( GL.ARRAY_BUFFER, bufferHandle );


        // We are uploading vertex buffer (GL_ARRAY_BUFFER)
        // and the vertex data will be uploaded once and rendered many times (GL_STATIC_DRAW)
        // Use GL_DYNAMIC_DRAW if the vertex data will be created once, changed from time to time, but drawn many times more than that
        // Use GL_STREAM_DRAW if the vertex data will be uploaded once and drawn once, it will change with every frame

        // This effectively uploads data to GPU memory
        // int numBytes = g_vertex_buffer_data.length * Float.BYTES;
        // gl.glBufferData(GL.GL_ARRAY_BUFFER, numBytes, bufferData, GL.GL_STATIC_DRAW);
	    gl.bufferData (GL.ARRAY_BUFFER, bufferData, GL.STATIC_DRAW);

	    gl.bindBuffer( GL.ARRAY_BUFFER, null );

	    return bufferHandle;
	}

    private function zoom( triangle_buffer_data : Array<Float>, factor:Float):Void {
	    for (i in 0...triangle_buffer_data.length) {
            triangle_buffer_data[ i ] *= factor;
        }
    }
}