package;


class XGLUtils {

    public static function zoom( triangle_buffer_data : Array<Float>, factor:Float ) : Void {
	    for( i in 0...triangle_buffer_data.length ) {
            triangle_buffer_data[ i ] *= factor;
        }
    }
}