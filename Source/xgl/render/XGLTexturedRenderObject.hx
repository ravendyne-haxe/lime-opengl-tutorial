package xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import core.Scene;

import xgl.shader.XGLTextureShader;

import xgl.material.XGLTexturedMaterial;

import xgl.geometry.XGLGeometryBufferUV;

import xgl.engine.XGLRenderObject;


class XGLTexturedRenderObject extends XGLRenderObject {

    override private function create( gl : WebGLRenderContext ) : Void {

        //
        // PROGRAM
        //

        shader = new XGLTextureShader( gl );

        //
        // GEOMETRY
        //

        geometryBuffer = new XGLGeometryBufferUV( gl, shader, mesh );

        //
        // MATERIAL
        //

        material = new XGLTexturedMaterial( gl, meshMaterial.textureImage );
    }

    override private function renderSetupMaterial( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        gl.uniform1i( shader.program.uniformHandles['texture'], material.texture.textureUnit );

        //
        //  Setup TEXTURE
        //
        gl.activeTexture( GL.TEXTURE0 + material.texture.textureUnit );
        gl.bindTexture( GL.TEXTURE_2D, material.texture.textureHandle );
    }
    
    override private function renderSetupGeometry( gl : WebGLRenderContext, scene : Scene ) : Void {

        //
        // Set UNIFORM values
        //
        var transform = mesh.transformMatrix.clone();
        transform.append( scene.projection );
        gl.uniformMatrix4fv( shader.program.uniformHandles['transformMatrix'], false, transform );

        //
        // Setup ATTRIBUTE values
        //
        gl.bindBuffer( GL.ARRAY_BUFFER, geometryBuffer.buffer.vertexBufferHandle );
        geometryBuffer.describeProgramAttributes( gl );
    }
    
    override private function renderDraw( gl : WebGLRenderContext ) : Void {

        //
        // Do DRAWING
        //
        gl.drawArrays( GL.TRIANGLES, 0, mesh.numberOfPrimitives );
    }
}

