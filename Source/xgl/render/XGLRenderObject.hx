package xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.math.RGBA;

import xgl.XGLColor;
import xgl.XGLShaderProgram;


class XGLRenderObject {

	public var shaderProgram : XGLShaderProgram;

	public function new( gl : WebGLRenderContext ) {

		init( gl );
	}

	private function init( gl : WebGLRenderContext ) : Void { }

	public function render( gl : WebGLRenderContext ) : Void {

		//
		// --- SETUP
		//

		//
		// ACTIVATE PROGRAM
		//
		gl.useProgram( shaderProgram.program );

		//
		// Enable PROGRAM attributes
		//
		for( attr in shaderProgram.attributes ) {
			gl.enableVertexAttribArray( attr );
		}


		//
		// --- RENDER
		//
		renderSetupGeometry( gl );
		renderSetupMaterial( gl );
		renderDraw( gl );



		//
		// --- CLEANUP
		//

		//
		// Disable PROGRAM attributes
		//
		for( attr in shaderProgram.attributes ) {
			gl.disableVertexAttribArray( attr );
		}
	}

	private function renderSetupGeometry( gl : WebGLRenderContext ) : Void { }

	private function renderSetupMaterial( gl : WebGLRenderContext ) : Void { }
	
	private function renderDraw( gl : WebGLRenderContext ) : Void { }
}
