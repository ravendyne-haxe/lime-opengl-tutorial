package xgl.render;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.math.Matrix4;

import xgl.material.XGLTexturedMaterial;

import xgl.shader.XGLShader;
import xgl.shader.XGLTextureShader;

import xgl.geometry.XGLGeometryBuffer;
import xgl.geometry.XGLGeometryBufferUV;

import xgl.render.XGLRenderObject;


class XGLQuadRenderObject extends XGLRenderObject {

	public var material : XGLTexturedMaterial;

	public var gb : XGLGeometryBuffer;
	public var shader : XGLShader;


	public var rotationMatrix : Matrix4;

	public function new( gl : WebGLRenderContext ) {

		super( gl );
	}

	override private function init( gl : WebGLRenderContext ) : Void {

		//
		// PROGRAM
		//

		shader = new XGLTextureShader( gl );
		shaderProgram = shader.program;

		//
		// GEOMETRY
		//

		// version with triangles
		// Use: gl.drawArrays( GL.TRIANGLES, 0, 6 );
        // var triangle_B_buffer_data : Array<Float> = [
		// 	// vertex 0
		// 	-0.5, -0.5, 0.0, 0.0, 1.0,
		// 	// vertex 1
		// 	-0.5, 0.5, 0.0, 0.0, 0.0,
		// 	// vertex 2
		// 	0.5, -0.5, 0.0, 1.0, 1.0,
		// 	// vertex 2
		// 	0.5, -0.5, 0.0, 1.0, 1.0,
		// 	// vertex 1
		// 	-0.5, 0.5, 0.0, 0.0, 0.0,
		// 	// vertex 3
		// 	0.5, 0.5, 0.0, 1.0, 0.0,
		// ];

		// version with triangle strips
		// Use: gl.drawArrays( GL.TRIANGLE_STRIP, 0, 4 );
        var triangle_B_buffer_data : Array<Float> = [
			// vertex 0
			-0.5, -0.5, 0.0, 0.0, 1.0,
			// vertex 1
			-0.5, 0.5, 0.0, 0.0, 0.0,
			// vertex 2
			0.5, -0.5, 0.0, 1.0, 1.0,
			// vertex 3
			0.5, 0.5, 0.0, 1.0, 0.0,
		];

		gb = new XGLGeometryBufferUV( gl, shader, triangle_B_buffer_data );

		//
		// MATERIAL
		//

		material = new XGLTexturedMaterial( gl, "assets/UV_Grid_1024.jpeg" );
	}

	override private function renderSetupMaterial( gl : WebGLRenderContext ) : Void {

		//
		// Set UNIFORM values
		//
		gl.uniformMatrix4fv( shaderProgram.uniforms['transformMatrix'], false, rotationMatrix );
		gl.uniform1i( shaderProgram.uniforms['texture'], material.texture.textureUnit );

		//
		//  Setup TEXTURE
		//
		gl.activeTexture( GL.TEXTURE0 + material.texture.textureUnit );
		gl.bindTexture( GL.TEXTURE_2D, material.texture.textureHandle );
	}
	
	override private function renderSetupGeometry( gl : WebGLRenderContext ) : Void {

		//
		// Setup ATTRIBUTE values
		//
		gl.bindBuffer( GL.ARRAY_BUFFER, gb.buffer.vertexBufferHandle );
		gb.describeProgramAttributes( gl );
	}
	
	override private function renderDraw( gl : WebGLRenderContext ) : Void {

		//
		// Do DRAWING
		//
		gl.drawArrays( GL.TRIANGLE_STRIP, 0, 4 );
	}
}

