package xgl.geometry;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.utils.Float32Array;

import xgl.engine.XGLArrayBuffer;

import xgl.engine.XGLGeometryBuffer;

import xgl.engine.XGLShader;

import core.Mesh;


class XGLGeometryBufferUV extends XGLGeometryBuffer {

    public function new( gl : WebGLRenderContext, shader : XGLShader, mesh : Mesh ) {

        super( gl, shader );

        // attribute format:
        // X, Y, Z, U, V -> number of elements == 5
		buffer = new XGLArrayBuffer( gl, createFromMesh( mesh ), 5 );
        // X, Y, Z -> number of elements == 3, offset == 0
		buffer.addAttribute( 'position', 3, 0 * Float32Array.BYTES_PER_ELEMENT );
        // U, V -> number of elements == 2, offset == 3
		buffer.addAttribute( 'aTextureUV', 2, 3 * Float32Array.BYTES_PER_ELEMENT );
    }

    private function createFromMesh( mesh : Mesh ) : Float32Array {

        var array = [];

        for( idx in 0...mesh.numberOfPrimitives ) {

            // NOT checking here if mesh.vertices and mesh.uvs
            // have same length, that is a requirement for well-formed Mesh
            // that wants to become an instance of XGLGeometryBufferUV
            var vertex = mesh.vertices[ idx ];
            var uv = mesh.uvs[ idx ];

            array.push( vertex[ 0 ] );
            array.push( vertex[ 1 ] );
            array.push( vertex[ 2 ] );
            array.push( uv[ 0 ] );
            array.push( uv[ 1 ] );
        }

        return new Float32Array( array );
    }

    override public function describeProgramAttributes( gl : WebGLRenderContext ) : Void {
        // describe where the value for this attribute is in the vertex data buffer
        // and what is it's structure
        gl.vertexAttribPointer(
            // attribute handle (ID)
            shader.program.attributeHandles['position'],
            // the number of components per generic vertex attribute. Must be 1, 2, 3, 4. 
            buffer.attributeParams['position'].componentCount,
            // the data type of each component in the array
            // The symbolic constants GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, and GL_UNSIGNED_INT are accepted by glVertexAttribPointer and glVertexAttribIPointer
            GL.FLOAT,
            // normalized or not
            false,
            // the byte offset between consecutive generic vertex attributes.
            // this is basically the number of bytes all attributes for one vertex take up
            buffer.vertexAttributesSize,
            // an offset of the first component of the first generic vertex attribute in the array buffer
            // "position" attribute starts right at the start - 0
            buffer.attributeParams['position'].attributeOffset
        );

        gl.vertexAttribPointer(
            shader.program.attributeHandles['aTextureUV'],
            buffer.attributeParams['aTextureUV'].componentCount,
            GL.FLOAT,
            false,
            buffer.vertexAttributesSize,
            buffer.attributeParams['aTextureUV'].attributeOffset
        );
    }
}
