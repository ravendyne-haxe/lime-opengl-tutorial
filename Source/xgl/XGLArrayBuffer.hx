package xgl;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLBuffer;
import lime.graphics.opengl.GLTexture;

import lime.utils.Float32Array;
import lime.utils.ArrayBufferView;


class XGLArrayBuffer {

    public var vertexBufferHandle : GLBuffer;
    public var geometryBufferData : Float32Array;

	// used for vertexAttribPointer
    public var vertexAttributesSize : Int;
    public var attributeParams : Map< String, XGLVertexAttributeParams >;
	// ------------------------------------------

    public function new( gl : WebGLRenderContext, bufferData : Float32Array, attributeSize : Int ) {

	    attributeParams = [];
	    geometryBufferData = bufferData;
	    vertexAttributesSize = attributeSize * Float32Array.BYTES_PER_ELEMENT;


	    vertexBufferHandle = gl.createBuffer();
        uploadVertexBuffer( gl, geometryBufferData );
	}

    private function uploadVertexBuffer( gl : WebGLRenderContext, bufferData : ArrayBufferView  ) : Void {

        // GL_ARRAY_BUFFER is for vertices
        // GL_ELEMENT_ARRAY_BUFFER is for geometry elements, etc.
        // gl.glBindBuffer( GL2ES2.GL_ARRAY_BUFFER, vertexBufferHandle );
	    gl.bindBuffer( GL.ARRAY_BUFFER, vertexBufferHandle );


        // Use GL_STATIC_DRAW if the vertex data will be uploaded once and rendered many times
        // Use GL_DYNAMIC_DRAW if the vertex data will be created once, changed from time to time, but drawn many times more than that
        // Use GL_STREAM_DRAW if the vertex data will be uploaded once and drawn once, it will change with every frame
	    gl.bufferData( GL.ARRAY_BUFFER, bufferData, GL.STATIC_DRAW );

        // when done, unbind
	    gl.bindBuffer( GL.ARRAY_BUFFER, null );
	}

    public function addAttribute( name : String, componentCount : Int, attributeOffset : Int ) : Void {

	    attributeParams[ name ] = new XGLVertexAttributeParams( componentCount, attributeOffset );
	}
}
