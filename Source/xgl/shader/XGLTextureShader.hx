package xgl.shader;

import lime.graphics.WebGLRenderContext;

import xgl.engine.XGLShaderProgram;

import xgl.engine.XGLShader;


class XGLTextureShader extends XGLShader {

    override private function createShader( gl : WebGLRenderContext ) {

		var vertexShaderSource = "
			// #version 120
			attribute vec3 position;
			attribute vec2 aTextureUV;

			uniform mat4 transformMatrix;

			varying vec2 vTextureUV;

			void main( void ) {
				vTextureUV = aTextureUV;
				gl_Position = transformMatrix * vec4(position, 1.0);
			}
		";

		var fragmentShaderSource = 
			#if (!desktop || rpi )
			"precision mediump float;" +
			#end
			"
			// #version 120
			varying vec2 vTextureUV;

			uniform sampler2D texture;

			void main( void ) {
				gl_FragColor = texture2D( texture, vTextureUV );
			}
		";

		program = new XGLShaderProgram( gl, vertexShaderSource, fragmentShaderSource, [ "position", "aTextureUV" ], [ "transformMatrix", "texture" ] );
    }
}