package xgl.material;

import lime.graphics.WebGLRenderContext;

import lime.math.RGBA;

import xgl.engine.XGLMaterial;


class XGLSolidColorMaterial extends XGLMaterial {

	public function new( gl : WebGLRenderContext, rgbaColor : RGBA ) {

		super( gl, rgbaColor );
	}
}
