package xgl.material;

import lime.graphics.WebGLRenderContext;
import lime.graphics.Image;

import lime.graphics.opengl.GLTexture;

import xgl.engine.XGLMaterial;
import xgl.engine.XGLTexture;


class XGLTexturedMaterial extends XGLMaterial {

	public var textureImageLocation : String;

	public function new( gl : WebGLRenderContext, textureImage : Image ) {

		super( gl );

		texture = new XGLTexture( gl, textureImage );
	}
}
