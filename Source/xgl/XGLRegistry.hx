package xgl;

import lime.graphics.WebGLRenderContext;

import core.SceneObject;

import core.material.BasicMaterial;
import core.material.TextureMaterial;

import xgl.render.XGLSolidColorRenderObject;
import xgl.render.XGLTexturedRenderObject;

import xgl.engine.XGLRenderObject;


class XGLRegistry {

    public static function createRenderObjectFor( gl : WebGLRenderContext, sceneObject : SceneObject ) : XGLRenderObject {

        var materialType = Type.typeof( sceneObject.material );


        if( Type.enumEq( materialType, TClass( BasicMaterial ) ) ) {

            return new XGLSolidColorRenderObject( gl, sceneObject );
        }

        if( Type.enumEq( materialType, TClass( TextureMaterial ) ) ) {

            return new XGLTexturedRenderObject( gl, sceneObject );
        }


        return null;
    }
}
