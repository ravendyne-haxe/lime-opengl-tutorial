package xgl.engine;

import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GLProgram;
import lime.graphics.opengl.GLUniformLocation;


class XGLShaderProgram {

	public var attributeHandles : Map< String, Int > = [];
	public var uniformHandles : Map< String, GLUniformLocation > = [];

	public var programHandle : GLProgram;

	public function new( gl : WebGLRenderContext, vertexSource : String, fragmentSource : String, attributeNames : Array< String >, uniformNames : Array< String > ) {

		programHandle = GLProgram.fromSources( gl, vertexSource, fragmentSource );

		for( attrName in attributeNames ) {

			attributeHandles[ attrName ] = gl.getAttribLocation( programHandle, attrName );
		}

		for( uniName in uniformNames ) {

			uniformHandles[ uniName ] = gl.getUniformLocation( programHandle, uniName );
		}
	}
}
