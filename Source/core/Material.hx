package core;

import lime.graphics.Image;

import lime.math.RGBA;


class Material {

    public var textureImage : Image;

    public var emissiveColor : RGBA;

    public function new() {
    }
}
