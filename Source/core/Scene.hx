package core;

import lime.graphics.WebGLRenderContext;

import lime.math.Matrix4;

import core.SceneObject;

import xgl.XGLRegistry;

import xgl.engine.XGLRenderObject;



class Scene {

    public var objects : Array< SceneObject >;
    public var projection : Matrix4;

    private var renderObjectList : Map< SceneObject, XGLRenderObject >;

    public function new() {

        objects = [];
        renderObjectList = [];

        projection = new Matrix4();
    }

    public function add( object : SceneObject ) {

        objects.push( object );
    }

    public function updateRenderObjectList( gl : WebGLRenderContext ) : Void {

        // A way to check if a Map is empty
        if( ! renderObjectList.iterator().hasNext() ) {

            // Initial list creation
            for( sceneObject in objects ) {

                renderObjectList[ sceneObject ] = XGLRegistry.createRenderObjectFor( gl, sceneObject );
            }
        }
    }

    public function getRenderObjectList( gl : WebGLRenderContext ) : Iterator< XGLRenderObject > {

        return renderObjectList.iterator();
    }
}
