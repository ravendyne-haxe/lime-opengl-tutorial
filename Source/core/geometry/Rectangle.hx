package core.geometry;

import lime.math.Vector4;

import core.SceneObject;
import core.Material;


class Rectangle extends SceneObject {

    public var position : Vector4;
    public var width : Float;
    public var height : Float;

    public function new( position : Vector4, width : Float = 1.0, height : Float = 1.0, ?material : Material) {

        super();

        this.position = position;
        this.height = height;
        this.width = width;

        if( material != null ) {
            this.material = material;
        }

        updateMesh();
    }

    public function updateMesh() : Void {

        mesh.vertices = [];
        mesh.uvs = [];

        var up = Vector4.X_AXIS;
        up.scaleBy( height );
        var right = Vector4.Y_AXIS;
        right.scaleBy( width );

/*
    v2 --- v3
    | \     |
    |  \    |
    |   \   |
    |    \  |
    v0 --- v1
*/
        var v0 = position;
        var v1 = v0.add( right );
        var v2 = v0.add( up );
        var v3 = v1.add( up );

        var uv0 = [ 0.0, 1.0 ];
        var uv1 = [ 0.0, 0.0 ];
        var uv2 = [ 1.0, 1.0 ];
        var uv3 = [ 1.0, 0.0 ];

        // triangle A
        // vertex 0
        mesh.vertices.push( [ v0.x, v0.y, v0.z ] );
        mesh.uvs.push( uv0 );
        // vertex 1
        mesh.vertices.push( [ v1.x, v1.y, v1.z ] );
        mesh.uvs.push( uv1 );
        // vertex 2
        mesh.vertices.push( [ v2.x, v2.y, v2.z ] );
        mesh.uvs.push( uv2 );

        // triangle B
        // vertex 2
        mesh.vertices.push( [ v2.x, v2.y, v2.z ] );
        mesh.uvs.push( uv2 );
        // vertex 1
        mesh.vertices.push( [ v1.x, v1.y, v1.z ] );
        mesh.uvs.push( uv1 );
        // vertex 3
        mesh.vertices.push( [ v3.x, v3.y, v3.z ] );
        mesh.uvs.push( uv3 );

    }
}
