package core.geometry;

import lime.math.Vector4;

import core.SceneObject;
import core.Material;


class Triangle extends SceneObject {

    public var p0 : Vector4;
    public var p1 : Vector4;
    public var p2 : Vector4;

    public function new( p0 : Vector4, p1 : Vector4, p2 : Vector4, ?material : Material ) {

        super();

        this.p0 = p0;
        this.p1 = p1;
        this.p2 = p2;

        if( material != null ) {
            this.material = material;
        }

        updateMesh();
    }

    public function updateMesh() : Void {

        mesh.vertices = [];
        mesh.uvs = [];

        mesh.vertices.push( [ p0.x, p0.y, p0.z ] );
        mesh.uvs.push( [ 0., 1. ] );

        mesh.vertices.push( [ p1.x, p1.y, p1.z ] );
        mesh.uvs.push( [ 0., 0. ] );

        mesh.vertices.push( [ p2.x, p2.y, p2.z ] );
        mesh.uvs.push( [ 1., 1. ] );
    }
}
