package core.geometry;

import lime.math.Vector4;

import core.Material;

import core.geometry.Rectangle;


class Square extends Rectangle {

    public var side : Float;

    public function new( position : Vector4, side : Float = 1.0, ?material : Material) {

        super( position, side, side, material );
    }
}
