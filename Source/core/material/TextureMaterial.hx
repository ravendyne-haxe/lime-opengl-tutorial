package core.material;

import lime.graphics.Image;

import lime.math.RGBA;

import lime.utils.Assets;

import core.Material;

class TextureMaterial extends Material {

    public function new( imageLocation : String ) {

        super();

		textureImage = Assets.getImage( imageLocation );
    }
}
