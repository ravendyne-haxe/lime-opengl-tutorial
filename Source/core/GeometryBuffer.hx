package core;

import lime.graphics.WebGLRenderContext;

import lime.utils.Float32Array;

import xgl.XGLArrayBuffer;


class GeometryBuffer {

	public var buffer : XGLArrayBuffer;

    public function new( gl : WebGLRenderContext, bufferData : Array<Float> ) {

        // attribute format:
        // X, Y, Z

		buffer = new XGLArrayBuffer( gl, new Float32Array( bufferData ), 3 );
		buffer.addAttribute( 'position', 3, 0 * Float32Array.BYTES_PER_ELEMENT );
    }
}
