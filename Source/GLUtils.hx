package;

import lime.graphics.WebGLRenderContext;
import lime.graphics.Image;

import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLBuffer;
import lime.graphics.opengl.GLTexture;

import lime.utils.ArrayBufferView;

class GLUtils {

    public static function createAndUploadTextureBuffer( gl : WebGLRenderContext, textureUnit : Int, image : Image ) : GLTexture {
        // no mipmap for this example
        var mipMap = false;
        var imageWidth = image.buffer.width;
        var imageHeight = image.buffer.height;
        // ByteBuffer pixelsBuffer = convertToRGBABuffer( image );
        var alpha = true;

        // must be one of GL_ALPHA, GL_RGB, GL_RGBA, GL_LUMINANCE, and GL_LUMINANCE_ALPHA
        var type = alpha ? GL.RGBA : GL.RGB;

	    var texture : GLTexture = gl.createTexture();

        // By default, glBindTexture binds our texture to texture unit 0
        // Just for the sake of clarity, we will explicitly set the texture unit to use, unit 0
        // this texture unit will be bound to the first declared sampler2D uniform in your fragment shader.
        gl.activeTexture( GL.TEXTURE0 + textureUnit );
	    gl.bindTexture( GL.TEXTURE_2D, texture );

        // when scaling down
        // use GL2ES2.GL_NEAREST or GL2ES2.GL_NEAREST_MIPMAP_LINEAR for pixelated look
        gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, mipMap ? GL.LINEAR_MIPMAP_LINEAR : GL.LINEAR );
        // when scaling up
        // use GL2ES2.GL_NEAREST or GL2ES2.GL_NEAREST_MIPMAP_LINEAR for pixelated look
        gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, mipMap ? GL.LINEAR_MIPMAP_LINEAR : GL.LINEAR );
		// gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
		// gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST );
		// gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE );
		// gl.texParameteri( GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE );

        // This effectively uploads the texture data (pixels) to GPU
        gl.texImage2D(
            GL.TEXTURE_2D, 
            0, 
            type,
            imageWidth, 
            imageHeight, 
            0, 
            type,
            GL.UNSIGNED_BYTE,
            image.data // pixelsBuffer
        );

        // generate mipmaps AFTER image data has been loaded to GPU
        if( mipMap ) {
            gl.generateMipmap( GL.TEXTURE_2D );
        }

	    gl.bindTexture( gl.TEXTURE_2D, null );

	    return texture;
	}

    public static function createAndUploadVertexBuffer( gl:WebGLRenderContext, bufferData : ArrayBufferView  ) : GLBuffer {

        // transfer vertex data to GPU memory
        // this way we don't have to keep buffers in RAM and transfer them on every frame render

        // Generate 1 buffer, put the resulting identifier in vertexbufferHandles
	    var bufferHandle:GLBuffer = gl.createBuffer(); // == glGenBuffers( 1, &bufferHandle )

        // this sets the type of data we want to upload to GPU
        // GL_ARRAY_BUFFER is for vertices
        // GL_ELEMENT_ARRAY_BUFFER is for geometry elements, etc.
        // gl.glBindBuffer( GL2ES2.GL_ARRAY_BUFFER, bufferHandle );
	    gl.bindBuffer( GL.ARRAY_BUFFER, bufferHandle );


        // We are uploading vertex buffer (GL_ARRAY_BUFFER)
        // and the vertex data will be uploaded once and rendered many times (GL_STATIC_DRAW)
        // Use GL_DYNAMIC_DRAW if the vertex data will be created once, changed from time to time, but drawn many times more than that
        // Use GL_STREAM_DRAW if the vertex data will be uploaded once and drawn once, it will change with every frame

        // This effectively uploads data to GPU memory
        // int numBytes = g_vertex_buffer_data.length * Float.BYTES;
        // gl.glBufferData( GL.GL_ARRAY_BUFFER, numBytes, bufferData, GL.GL_STATIC_DRAW);
	    gl.bufferData( GL.ARRAY_BUFFER, bufferData, GL.STATIC_DRAW );

	    gl.bindBuffer( GL.ARRAY_BUFFER, null );

	    return bufferHandle;
	}

    public static function zoom( triangle_buffer_data : Array<Float>, factor:Float ) : Void {
	    for( i in 0...triangle_buffer_data.length ) {
            triangle_buffer_data[ i ] *= factor;
        }
    }
}