package;

import lime.app.Application;
import lime.utils.Log;

import lime.graphics.RenderContext;
import lime.graphics.WebGLRenderContext;

import lime.graphics.opengl.GL;

import lime.math.Vector4;
import lime.math.Matrix4;

// import lime.utils.Assets;
// import lime.utils.AssetType;


import xgl.engine.XGLRenderer;


import core.Mesh;
import core.material.TextureMaterial;
import core.material.BasicMaterial;
import core.geometry.Triangle;
import core.geometry.Rectangle;
import core.geometry.Square;

import core.Scene;


class Main extends Application {

    private var minX : Int;
    private var minY : Int;
    private var maxX : Int;
    private var maxY : Int;

    private var rotationPointSquare : Vector4;
    private var rotationPointSquareInverse: Vector4;

    private var identityMatrix : Matrix4;

    private var renderer : XGLRenderer;
    private var orthoProjection : Matrix4;

    private var square : Square;
    private var rectangle : Rectangle;
    private var triangle : Triangle;

    private var scene : Scene;

    public function new() {

        super();

        trace("OpenGL Tutorial #14");
        Log.info("OpenGL Tutorial #14");
    }

    public override function onRenderContextLost() : Void {
        trace("onRenderContextLost");
    }

    public override function onRenderContextRestored( context:RenderContext ) : Void {
        trace("onRenderContextRestored");
    }

    public override function onPreloadComplete() : Void {
        trace("onPreloadComplete");
        // trace( Assets.list( AssetType.IMAGE ));

        init();
    }

    private function init() : Void {

        initApp();


        switch( window.context.type ) {

            case CAIRO:
                Log.error("Render context not supported: CAIRO");
                window.close();

            case CANVAS:
                Log.error("Render context not supported: CANVAS");
                window.close();

            case DOM:
                Log.error("Render context not supported: DOM");
                window.close();

            case FLASH:
                Log.error("Render context not supported: FLASH");
                window.close();

            case OPENGL, OPENGLES, WEBGL:

            #if ( lime_opengl || lime_opengles )
                trace( "OpenGL version: " + GL.getString( GL.VERSION ) );
                trace( "GLSL version: " + GL.getString( GL.SHADING_LANGUAGE_VERSION ) );
            #end

                var gl = window.context.webgl;

                renderer = new XGLRenderer( gl );

                initScene( gl );

            default:

                Log.error("Current render context not supported by this sample");
                window.close();

        }
    }

    private function initApp() : Void {

        // set window size here or (better yet) in project.xml: <window width="600" height="600" />
        // window.width = 600;
        // window.height = 600;

        minX = 0;
        maxX = window.width;
        minY = 0;
        maxY = window.height;


        // by default, new Matrix4 is set to identity matrix
        identityMatrix = new Matrix4();

        rotationPointSquare = new Vector4();
        rotationPointSquareInverse = new Vector4();
    }

    private function setupProjection() {

        // var left:Float = 0;
        // var right:Float = 2000; // width
        // var bottom:Float = 0;
        // var top:Float = 2000; // height
        var left:Float = -1000;
        var right:Float = 1000;
        var bottom:Float = -1000;
        var top:Float = 1000;
        var zNear:Float = -1000;
        var zFar:Float = 1000;
        orthoProjection = new Matrix4();
        orthoProjection.createOrtho( left, right, bottom, top, zNear, zFar );
        // Flip Y-axis
        // orthoProjection.createOrtho( left, right, top, bottom, zNear, zFar );
        // Flip Z-axis
        // orthoProjection.createOrtho( left, right, bottom, top, zFar, zNear );
    }

    private function test() {


        var left:Float = 0;
        var right:Float = 2000; // width
        var bottom:Float = 0;
        var top:Float = 2000; // height
        var zNear:Float = -1000;
        var zFar:Float = 1000;
        var orthoProjection = new Matrix4();
        orthoProjection.createOrtho( left, right, bottom, top, zNear, zFar );
        // orthoProjectionFlipped.createOrtho( left, right, top, bottom, zNear, zFar );

        var pointInSpace = new Vector4( 100, 100, 0 );
        trace( 'pointInSpace' );
        trace( pointInSpace );

        var point = new Matrix4();
        point.position = pointInSpace;
        point.append( orthoProjection );

        trace( 'point.position' );
        trace( point.position );
    }

    private function initScene( gl : WebGLRenderContext ) : Void {

        setupProjection();
        // test();

		//
		// SCENE
		//

        scene = new Scene();
        scene.projection = orthoProjection;


		//
		// SQUARE
		//
        square = new Square( new Vector4( -500., -500., 10.0 ), 1000., new TextureMaterial( "assets/UV_Grid_1024.jpeg" ) );
        scene.add( square );

        // sets rotation point to lower left corner
        rotationPointSquare = square.position.clone();
        rotationPointSquareInverse = rotationPointSquare.clone();
        rotationPointSquareInverse.negate();


		//
		// RECTANGLE
		//
        rectangle = new Rectangle( new Vector4( 650., 650., 0.0 ), 300., 200., new BasicMaterial() );
        scene.add( rectangle );


		//
		// TRIANGLE
		//
        triangle = new Triangle(
            // vertex 0
            new Vector4( -500., -500., 0.0 ),
            // vertex 1
            new Vector4( -500., 500., 0.0 ),
            // vertex 2
            new Vector4( 500., -500., 0.0 ),
            // lime color
            new BasicMaterial( 0x6fac17ff )
        );
        scene.add( triangle );
    }

    public override function update( deltaTime : Int ) : Void {

        if( !preloader.complete ) return;

        // deltaTime is in milliseconds
        var rotationSpeed = 90.0; // in degrees per second
        var delta = deltaTime / 1000.0;

        square.translate( rotationPointSquareInverse );
        square.rotate( delta * rotationSpeed, Vector4.Z_AXIS );
        square.translate( rotationPointSquare );
    }

    public override function render( context : RenderContext ) : Void {

        if( !preloader.complete ) return;

        switch( context.type ) {

            case OPENGL, OPENGLES, WEBGL:

                var gl = context.webgl;

                renderer.render( gl, scene );
            default:

        }
    }
}
